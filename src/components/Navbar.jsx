import React, { useState } from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  const navbarOpen = false;
  const [menuIsOpen, setMenuIsOpen] = useState(false);

  return (
    <>
      <nav
        className={
          "top-0 absolute z-50 w-full bg-white shadow-lg" +
          " flex flex-wrap items-center justify-between px-2 py-3 "
        }
      >
        <div className="container flex flex-wrap items-center justify-between px-4 mx-auto">
          <div className="relative flex justify-between w-full lg:w-auto lg:static lg:block lg:justify-start">
            <a
              href="/"
              className="inline-block py-2 mr-4 text-sm font-bold leading-relaxed text-gray-800 uppercase whitespace-nowrap"
            >
              LPMIAW Gallery
            </a>
            <button
              onClick={() => setMenuIsOpen(!menuIsOpen)}
              className="block px-3 py-1 text-xl leading-none bg-transparent border border-transparent border-solid rounded outline-none cursor-pointer lg:hidden focus:outline-none"
              type="button"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-6 h-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16M4 18h16"
                />
              </svg>
            </button>
          </div>
          <div
            className={
              "lg:flex flex-grow items-center bg-white lg:bg-transparent lg:shadow-none " +
              (menuIsOpen ? "block" : "hidden")
            }
            id="example-navbar-warning"
          >
            <ul className="flex flex-col mr-auto list-none lg:flex-row">
              <Link
                className="flex items-center px-3 py-4 text-xs font-bold text-gray-800 uppercase hover:text-gray-600 lg:py-2"
                to={"/"}
              >
                Accueil
              </Link>

              <Link
                className="flex items-center px-3 py-4 text-xs font-bold text-gray-800 uppercase hover:text-gray-600 lg:py-2"
                to={"/gallery"}
              >
                Galerie
              </Link>

              <Link
                className="flex items-center px-3 py-4 text-xs font-bold text-gray-800 uppercase hover:text-gray-600 lg:py-2"
                to={"/upload"}
              >
                Ajouter
              </Link>
            </ul>
            <ul className="flex flex-col list-none lg:flex-row lg:ml-auto">
              <Link
                className="px-4 py-2 mb-3 ml-3 text-xs font-bold text-white uppercase bg-pink-500 rounded shadow outline-none active:bg-pink-600 hover:shadow-md focus:outline-none lg:mr-1 lg:mb-0"
                to={"/login"}
              >
                Connexion
              </Link>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
