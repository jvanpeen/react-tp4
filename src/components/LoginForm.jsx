import React, { useState } from "react";
import PropTypes from "prop-types";

const LoginForm = (props) => {
  const { disabled, onSubmit } = props;

  const [formState, setFormState] = useState({
    username: "",
    password: "",
  });

  const handleFormChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();
    return onSubmit(formState);
  };

  return (
    <div className="relative flex flex-col w-full min-w-0 mb-6 break-words bg-gray-300 border-0 rounded-lg shadow-lg">
      <div className="flex-auto px-4 py-10 pt-4 lg:px-10">
        <form onSubmit={handleSubmit}>
          <div className="relative w-full mb-3">
            <label
              className="block mb-2 text-xs font-bold text-gray-700 uppercase"
              htmlFor="username"
            >
              Email
            </label>

            <input
              type="email"
              id="username"
              className="w-full px-3 py-3 text-sm text-gray-700 placeholder-gray-400 bg-white border-0 rounded shadow focus:outline-none focus:ring"
              placeholder="Email"
              style={{ transition: "all .15s ease" }}
              name="username"
              value={formState.username}
              onChange={handleFormChange}
              disabled={disabled}
            />
          </div>

          <div className="relative w-full mb-3">
            <label
              className="block mb-2 text-xs font-bold text-gray-700 uppercase"
              htmlFor="password"
            >
              Mot de passe
            </label>
            <input
              type="password"
              className="w-full px-3 py-3 text-sm text-gray-700 placeholder-gray-400 bg-white border-0 rounded shadow focus:outline-none focus:ring"
              placeholder="Mot de passe"
              style={{ transition: "all .15s ease" }}
              name="password"
              id="password"
              value={formState.password}
              onChange={handleFormChange}
              disabled={disabled}
            />
          </div>

          <div className="mt-6 text-center">
            <input
              type="submit"
              onSubmit={handleSubmit}
              className="w-full px-6 py-3 mb-1 mr-1 text-sm font-bold text-white uppercase bg-gray-900 rounded shadow outline-none active:bg-gray-700 hover:shadow-lg focus:outline-none"
              style={{ transition: "all .15s ease" }}
              disabled={disabled}
              value="Connexion"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

LoginForm.propTypes = {
  disabled: PropTypes.bool,
  onSubmit: PropTypes.func,
};

export default LoginForm;
