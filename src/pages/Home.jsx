import React from "react";

import heroImg from "../assets/pine-watt-2Hzmz15wGik-unsplash.jpg";
import gsdPhoto from "../assets/photo-1555212697-194d092e3b8f.jpg";

const Home = () => {
  return (
    <main>
      <div
        className="relative flex items-center content-center justify-center pt-16 pb-32"
        style={{
          minHeight: "75vh",
        }}
      >
        <div
          className="absolute top-0 w-full h-full bg-center bg-cover"
          style={{
            backgroundImage: `url('${heroImg}')`,
          }}
        >
          <span
            id="blackOverlay"
            className="absolute w-full h-full bg-black opacity-75"
          ></span>
        </div>
        <div className="container relative mx-auto">
          <div className="flex flex-wrap items-center">
            <div className="w-full px-4 ml-auto mr-auto text-center lg:w-6/12">
              <div className="pr-12">
                <h1 className="text-5xl font-semibold text-white">
                  Votre galerie photo
                </h1>
                <p className="mt-4 text-lg text-gray-300">
                  Un exemple d'application à réaliser avec React lors du module
                  Javascript Avancé de la Licence Professionelle MIAW de La
                  Rochelle Université.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          className="absolute bottom-0 left-0 right-0 top-auto w-full overflow-hidden pointer-events-none"
          style={{ height: "70px" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-gray-300 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
      </div>

      <section className="pb-20 -mt-24 bg-gray-300">
        <div className="container px-4 mx-auto">
          <div className="flex flex-wrap">
            <div className="w-full px-4 pt-6 text-center lg:pt-12 md:w-4/12">
              <div className="relative flex flex-col w-full min-w-0 mb-8 break-words bg-white rounded-lg shadow-lg">
                <div className="flex-auto px-4 py-5">
                  <div className="inline-flex items-center justify-center w-12 h-12 p-3 mb-5 text-center text-white bg-red-400 rounded-full shadow-lg">
                    <i className="fas fa-award"></i>
                  </div>
                  <h6 className="text-xl font-semibold">Gratuit</h6>
                  <p className="mt-2 mb-4 text-gray-600">
                    Ne payez jamais pour héberger vos photos
                  </p>
                </div>
              </div>
            </div>

            <div className="w-full px-4 text-center md:w-4/12">
              <div className="relative flex flex-col w-full min-w-0 mb-8 break-words bg-white rounded-lg shadow-lg">
                <div className="flex-auto px-4 py-5">
                  <div className="inline-flex items-center justify-center w-12 h-12 p-3 mb-5 text-center text-white bg-blue-400 rounded-full shadow-lg">
                    <i className="fas fa-retweet"></i>
                  </div>
                  <h6 className="text-xl font-semibold">Public</h6>
                  <p className="mt-2 mb-4 text-gray-600">
                    Tout le monde peut voir vos précieuses photos
                  </p>
                </div>
              </div>
            </div>

            <div className="w-full px-4 pt-6 text-center md:w-4/12">
              <div className="relative flex flex-col w-full min-w-0 mb-8 break-words bg-white rounded-lg shadow-lg">
                <div className="flex-auto px-4 py-5">
                  <div className="inline-flex items-center justify-center w-12 h-12 p-3 mb-5 text-center text-white bg-green-400 rounded-full shadow-lg">
                    <i className="fas fa-fingerprint"></i>
                  </div>
                  <h6 className="text-xl font-semibold">Un super exercice</h6>
                  <p className="mt-2 mb-4 text-gray-600">
                    Un exercice semblable à un projet que vous pourriez faire
                    dans la vraie vie.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="relative py-20">
        <div
          className="absolute top-0 left-0 right-0 bottom-auto w-full -mt-20 overflow-hidden pointer-events-none"
          style={{ height: "80px" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-white fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>

        <div className="container px-4 mx-auto">
          <div className="flex flex-wrap items-center">
            <div className="w-full px-4 ml-auto mr-auto md:w-4/12">
              <img
                alt="..."
                className="max-w-full rounded-lg shadow-lg"
                src={gsdPhoto}
              />
            </div>
            <div className="w-full px-4 ml-auto mr-auto md:w-5/12">
              <div className="md:pr-12">
                <h3 className="mt-6 text-3xl font-semibold">Au boulot</h3>
                <p className="mt-4 text-lg leading-relaxed text-gray-600">
                  Allez, on arrête de lire la page marketing et on se met au
                  travail, ce TP ne va pas se faire tout seul.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="relative block pb-20 bg-gray-900">
        <div
          className="absolute top-0 left-0 right-0 bottom-auto w-full -mt-20 overflow-hidden pointer-events-none"
          style={{ height: "80px" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-gray-900 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
      </section>
      <section className="relative block py-24 bg-gray-900 lg:pt-12">
        <div className="container px-4 mx-auto">
          <div className="flex flex-wrap justify-center -mt-48">
            <div className="w-full px-4 lg:w-6/12">
              <div className="relative flex flex-col w-full min-w-0 mb-6 break-words bg-gray-300 rounded-lg shadow-lg">
                <div className="flex-auto p-5 lg:p-10">
                  <h4 className="text-2xl font-semibold">
                    Vous voulez essayer ?
                  </h4>
                  <p className="mt-1 mb-4 leading-relaxed text-gray-600">
                    Connectez vous pour ajouter une image à la galerie.
                  </p>

                  <div className="mt-6 text-center">
                    <a href="/login">
                      <button
                        className="px-6 py-3 mb-1 mr-1 text-sm font-bold text-white uppercase bg-gray-900 rounded shadow outline-none active:bg-gray-700 hover:shadow-lg focus:outline-none"
                        type="button"
                        style={{ transition: "all .15s ease" }}
                      >
                        Connexion
                      </button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
};

export default Home;
