import { React, useEffect, useState } from "react";
import Fab from "../components/Fab";
import Thumbnail from "../components/Thumbnail";
import { Link } from "react-router-dom";
// import "../../db.json";

const Gallery = () => {
  const [listeImages, setListeImages] = useState([]);
  useEffect(() => {
    imagesData();
  }, []);

  const imagesData = () =>
    fetch("http://localhost:3000/api/images")
      .then((res) => res.json())
      .then((data) => {
        setListeImages(data);
        // console.log(data);
        console.log(data);
      });

  const images = [
    {
      id: "1000",
      author: "0",
      url: "https://picsum.photos/id/1000/2500/2500",
      description: "Sample from Unsplash",
    },
    {
      id: "1001",
      author: "0",
      url: "https://picsum.photos/id/1001/2500/2500",
      description: "Sample from Unsplash",
    },
    {
      id: "1002",
      author: "0",
      url: "https://picsum.photos/id/1002/4312/2868",
      description: "Sample from Unsplash",
    },
  ];

  return (
    <div className="mx-auto">
      <ul className="grid grid-cols-1 gap-1 mx-auto sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6">
        {listeImages.map((image) => {
          return (
            <li>
              <Thumbnail url={image.url} description={image.description} />
            </li>
          );
        })}
        <li>
          <Thumbnail
            url="https://picsum.photos/id/1000/2500/2500"
            description="Sample from Unsplash"
          />
        </li>
      </ul>
      <Link to={"/upload"}>
        <Fab icon="+" />
      </Link>
    </div>
  );
};

export default Gallery;
