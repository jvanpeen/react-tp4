import React from "react";
import LoginForm from "../components/LoginForm";

const Login = () => {
  return (
    <div className="container h-full px-4 mx-auto">
      <div className="flex items-center content-center justify-center h-full">
        <div className="w-full px-4 lg:w-4/12">
          {/* Mettez l'alerte ici ! */}
          <LoginForm
            onSubmit={(credentials) => console.log(credentials)}
            disabled={false}
          />
        </div>
      </div>
    </div>
  );
};

export default Login;
